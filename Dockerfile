FROM centos
RUN cd /etc/yum.repos.d && \
  echo "[mongodb-org-3.2]" >> mongodb-org-3.2.repo && \
  echo "name=MongoDB Repository" >> mongodb-org-3.2.repo && \
  echo "baseurl=https://repo.mongodb.org/yum/redhat/7/mongodb-org/3.2/x86_64/" >> mongodb-org-3.2.repo && \
  echo "gpgcheck=1" >> mongodb-org-3.2.repo && \
  echo "enabled=1" >> mongodb-org-3.2.repo && \
  echo "gpgkey=https://www.mongodb.org/static/pgp/server-3.2.asc" >> mongodb-org-3.2.repo && \
  echo "[mariadb]" >> MariaDB.repo && \
  echo "name = MariaDB" >> MariaDB.repo && \
  echo "baseurl = http://yum.mariadb.org/10.1/centos7-amd64" >> MariaDB.repo && \
  echo "gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB" >> MariaDB.repo && \
  echo "gpgcheck=1" >> MariaDB.repo && \
  yum update -y && \
  yum install -y mongodb-org && \
  yum install -y MariaDB-server && \
  yum install -y java-1.8.0-openjdk-devel && \
  yum install -y maven && \
  yum clean all && \
  cd /opt && \
  mkdir payara && \
  cd payara && \
  curl -o payara-micro.jar https://s3-eu-west-1.amazonaws.com/payara.co/Payara+Downloads/Payara+4.1.1.163/payara-micro-4.1.1.163.jar

RUN cd /var/tmp && \
  mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll -DarchetypeArtifactId=java-ee-project -DarchetypeVersion=1.2.0 -DgroupId=edu.byu.hbll -DartifactId=project-test -Dversion=1.0.0 -Dpackage=edu.byu.hbll.projecttest -DarchetypeRepository=https://maven.lib.byu.edu/repository -DinteractiveMode=false && \
  cd project-test && \
  mvn clean package && \
  (java -jar /opt/payara/payara-micro.jar --logToFile /var/log/payara-micro.log --port 8080 --deploy target/*.war & echo $! >> /var/tmp/payara.pid) && \
  (while ! grep -P -m1 'Payara Micro .*? ready' < /var/log/payara-micro.log; do sleep 1; done) && \
  mvn verify && \
  cd .. && \
  rm -rf project-test && \
# This seems to be the only way to start mongodb and mariadb when the container starts.
# I don't particularly like it, but it works in the absence of better options.
  echo "mongod --config /etc/mongod.conf" >> /root/.bashrc && \
  echo "/etc/init.d/mysql start" >> /root/.bashrc && \
  kill $(</var/tmp/payara.pid) && \
  rm /var/log/payara-micro.log && \
  touch /var/log/payara-micro.log && \
  rm /var/tmp/payara.pid && \
  rm -rf /var/log/project-test
  



ENTRYPOINT ["/bin/bash"]

