package edu.byu.hbll.citest.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

/**
 * Created by bwelker on 11/9/16.
 */
@Path("https")
public class HttpsAPI {

    private static Client client = ClientBuilder.newClient();

    @GET
    @Path("1")
    public Response doFirstHttps(){
        return Response.ok().entity(client.target("https://search.lib.byu.edu/green/byu/record/lee.2554791").request().get(String.class)).build();
    }

    @GET
    @Path("2")
    public Response doSecondHttps(){
        return Response.ok().entity(client.target("https://www.google.com/").request().get(String.class)).build();
    }


}
