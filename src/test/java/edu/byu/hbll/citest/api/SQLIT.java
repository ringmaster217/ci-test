package edu.byu.hbll.citest.api;

import edu.byu.hbll.db.TemporaryDatabase;
import org.apache.commons.dbutils.QueryRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.sql.DataSource;

/**
 * Created by bwelker on 10/26/16.
 */
public class SQLIT {

    private static TemporaryDatabase.Builder tempDbBuilder;
    private TemporaryDatabase tempDb;

    @BeforeClass
    public static void beforeAll(){
        tempDbBuilder = new TemporaryDatabase.Builder()
                .user("root")
                .password("");
    }

    @Before
    public void beforeTest() throws Exception{
        tempDb = tempDbBuilder.build("CREATE TABLE IF NOT EXISTS `test` ("
                + "`id` int(11) NOT NULL AUTO_INCREMENT,"
                + "`val` varchar(255),"
                + "PRIMARY KEY (`id`)"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
    }

    @After
    public void afterTest() throws Exception{
        tempDb.close();
    }

    @Test
    public void shouldConnectToServer() throws Exception{
        DataSource ds = tempDb.getDataSource();
        QueryRunner runner = new QueryRunner(ds);
        runner.update("INSERT INTO `test` (`val`) VALUES(7);");
    }
}
