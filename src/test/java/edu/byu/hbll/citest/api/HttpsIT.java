package edu.byu.hbll.citest.api;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

/**
 * Created by bwelker on 11/9/16.
 */
public class HttpsIT {

    private static Client client = ClientBuilder.newClient();

    @Test
    public void testHttpFromJunit(){
        Response response = client.target("https://search.lib.byu.edu/green/byu/record/lee.2554791").request().get();
        assertEquals(200, response.getStatus());
        response = client.target("https://www.google.com/").request().get();
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testByuHttpsCall(){
        Response response = client.target("http://localhost:8080/citest/https/1").request().get();
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testGoogleHttpsCall(){
        Response response = client.target("http://localhost:8080/citest/https/2").request().get();
        assertEquals(200, response.getStatus());
    }
}
