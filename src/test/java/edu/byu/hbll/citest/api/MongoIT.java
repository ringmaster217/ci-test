package edu.byu.hbll.citest.api;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by bwelker on 10/20/16.
 */
public class MongoIT {

    @Test
    public void testMongoStuff(){
        MongoClient client = new MongoClient();
        MongoDatabase db =client.getDatabase("teststuffhere");
        MongoCollection<Document> collection = db.getCollection("testcollectionhere");
        for(int i = 0; i < 1000; i++){
            collection.insertOne(new Document().append("_id", i));
        }
        assertEquals(1000, collection.count());
    }
}
